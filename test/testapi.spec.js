require('dotenv').config();
const jwt = require('jsonwebtoken');
const assert = require('assert');
const fetch = require('node-fetch');

describe(`probando unas APIs`, () => {

  it(`API responde 200`, async () => {
    const url = `https://jsonplaceholder.typicode.com/todos/1`;
    await fetch(url)
    .then(response => {
      assert.equal(response.status, 200);
    });
  });

  it(`API devuelve title: "delectus aut autem`, async () => {
    const url = `https://jsonplaceholder.typicode.com/todos/1`;
    await fetch(url)
    .then(response => response.json())
    .then(data => {
      assert.equal(data.title, 'delectus aut autem');
    });
  });


  it(`API devuelve un token`, async () => {
    const url = `http://localhost:3022/login`;
    await fetch(url, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({ username : 'admin', password: 'clavesegura'})
    })
    .then(response => response.json())
      .then(data => {
        const decoded = jwt.verify(data.yourToken, process.env.JWT_SECRET);
        assert.equal(decoded.user, 'admin');
      });
  });


  it(`API responde 404`, async () => {
    const url = `http://localhost:3022/prueba`;
    await fetch(url)
    .then(response => {
      assert.equal(response.status, 404);
    });
  });

});
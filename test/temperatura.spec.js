const assert = require('assert');
const fetch = require('node-fetch');

const createUrl = (city) => `https://api.openweathermap.org/data/2.5/weather?q=${city},%20argentina&appid=1c67ae33adc312d33476ae4d68a09beb&mode=json&units=metric`;

const bsAs = createUrl('buenos%20aires');

const fakeCity = createUrl('tangamandapio');

describe(`Pruebas API openweathermap`, () => {
  it(`API responde con un 200`, async () => {
    await fetch(bsAs)
      .then(response => {
        assert.equal(response.status, 200);
      })
  });

  it(`API responde con el clima de Bs As`, async () => {
    await fetch(bsAs)
      .then(response => response.json())
      .then(data => {
        assert.equal(data.name, 'Buenos Aires');
      });
  });

  it(`API responde con 404`, async () => {
    await fetch(fakeCity)
      .then(response => {
        assert.equal(response.status , 404);
      });
  });

});

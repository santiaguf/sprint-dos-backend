const assert = require('assert');
const calculadora = require('../src/pruebas/index');

describe('testeando funcionalidades básicas', () => {

  it('debería retornar -1 si no encuentra el número 5 en el array', () => {
    assert.equal([1,2,3].indexOf(5), -1);
  });

  it('debería retornar ok, para validar si Year es igual a 2021', () => {
    const year = 2021;
    assert.strictEqual(year, 2021);
  });

  before(() => {
    console.log('before tests');
  });

  after(() => {
    console.log('after tests');
  });

});

describe('testeando la calculadora', () => {

  it('debería retornar 5 si sumo 2 y 3', () => {
    assert.strictEqual(calculadora.sum(2,3),5);
  });

  it(`debería retornar 20 si multiplico 4 y 5`, () => {
    assert.strictEqual(calculadora.mult(4,5),20);
  });

});
const sum = (a, b) => a + b;
const rest = (a, b) => a - b;
const mult = (a, b) => a * b;

module.exports = {
  sum,
  rest,
  mult
};
const express = require('express');
const funciones = require('./functions');
const ciudades = require('./ciudades');
const app = express();
const port = 3021;
const host = 'http://localhost';
const url = `${host}:${port}`;

app.use(express.urlencoded({ extended: true }));
app.use(express.json());

app.get('/', function (req, res) {
    res.json(funciones.getCities(ciudades));
});

app.get('/:city', function (req, res) {
  const cityName = req.params.city;
  const result = funciones.getWeatherByCityName(cityName);

  result
  .then(result =>  res.json(result))
  .catch(err => res.json(err));
});

app.get('/random', function (req, res) {
  funciones.get3Cities(ciudades)
    .then(function (data) {
      res.json(data);
    })
    .catch(function (err) {
      res.json(err);
    });
});

app.listen(port, () => {
    console.log(`Servidor iniciado en ${url}`);
});
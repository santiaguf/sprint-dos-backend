
require('dotenv').config();
const fetch = require('node-fetch');
const redis = require('redis');
const bluebird = require('bluebird');
bluebird.promisifyAll(redis);
const client = redis.createClient();
const getRandomNumber = (length) => Math.floor(Math.random() * length);
const URL = `https://api.openweathermap.org/data/2.5/weather?q=`;
const apiKey = process.env.OPEN_WEATHER_MAP_API_KEY;

const getWeather = (city) => {
  const api = `${URL}${city}&appid=${apiKey}`;
  return fetch(api)
  .then(res => res.json())
  .then(data => {
    const temperatureInCelsius = (data.main[`temp`] - 273.15).toFixed(1);
    const fakeCity = { 'name': data.name, 'weather': temperatureInCelsius }
    return fakeCity})
  .catch(err => console.log(err));
};

const getCities = (cities) => cities;

const getWeatherByCityName = async (cityName) => {

  const cityOnRedis = await client.getAsync(cityName);

  if(cityOnRedis !== null){
    return JSON.parse(cityOnRedis);
  }else{
    const result = getWeather(cityName)
    .then((res) => {
      client.set(cityName , JSON.stringify(res), 'EX', 60*20);
      return res;
    })
    .catch((err) => {
      return err});
    return result;
  }
}

const get3Cities  = (cities) => {
  return new Promise((resolve, reject) => {
    let promises = [];
    for (let i = 0; i < 3; i++) {
      const randomCity = cities[getRandomNumber(cities.length)];
      promises[i] = getWeather(randomCity);
    }
    resolve(Promise.all(promises));
    reject('error');
  });
};

module.exports = {
  getCities,
  get3Cities,
  getWeatherByCityName
}
const express = require('express')
const sql = require('./mysql');

const app = express();
const port = 3031;

app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request-Method');
  res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE');
  res.header('Allow', 'GET, POST, OPTIONS, PUT, DELETE');
  next();
});



app.get('/', (req, res) => {
  res.send('API! en slash')
});

app.get('/students', async (req, res) => {
  try {
    // Esperamos a que termine de devolver los estudiantes
    // antes de retornar una respuesta
    const students = await sql.getAllStudents();
    res.send(students);
  } catch (err) {
    console.error(`Error: `, err.message);
  }
})


app.get('/api', (req, res) => {
  res.send('¡Esta es información obtenida desde tu API!')
});

app.listen(port, () => {
  console.log(`Aplicación escuchando en puerto: ${port}`)
})
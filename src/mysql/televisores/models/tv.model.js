const Televisormodel = (connection, Sequelize) => {
  const Televisor = connection.define('televisores', {
      id_televisor: {
        type: Sequelize.INTEGER,
        primaryKey: true
      },
      marca_televisor: {
        type: Sequelize.STRING
      },
      modelo_televisor: {
        type: Sequelize.STRING
      },
      precio: {
        type: Sequelize.INTEGER
      },
      tamano_pantalla: {
        type: Sequelize.INTEGER
      },
      smart_tv: {
        type: Sequelize.BOOLEAN
      }
  },
  {
    timestamps: false
  });
  return Televisor
}

module.exports = Televisormodel;
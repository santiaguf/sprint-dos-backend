--
-- Estructura de tabla para la tabla `televisores`
--

CREATE TABLE `televisores` (
  `id_televisor` int(4) NOT NULL,
  `marca_televisor` varchar(25) NOT NULL,
  `modelo_televisor` varchar(25) NOT NULL,
  `precio` int(4) NOT NULL,
  `tamano_pantalla` int(2) NOT NULL,
  `smart_tv` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `televisores`
--

INSERT INTO `televisores` (`id_televisor`, `marca_televisor`, `modelo_televisor`, `precio`, `tamano_pantalla`, `smart_tv`) VALUES
(1, 'Samsung', 'XE65UHDX', 1000, 65, 1),
(2, 'lg', 'HK 10', 700, 50, 1);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `televisores`
--
ALTER TABLE `televisores`
  ADD PRIMARY KEY (`id_televisor`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `televisores`
--
ALTER TABLE `televisores`
  MODIFY `id_televisor` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;
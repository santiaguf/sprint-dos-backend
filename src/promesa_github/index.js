const API = 'https://api.github.com/users/';

const getFirst5Followers = (url) => {
  fetch(url)
  .then(response => response.json())
  .then(json => {
    console.log(json);
    for(let i=0; i< 5; i++){
      console.log(json[i].login);
    }
  })
  .then(() => { console.log(`lunes`)})
  .catch(err => console.log(`falló: ${err.message}`))
}

const github = (username) => {
  const url = API + username;
  fetch(url)
  .then(response => response.json())
  .then(json => {
    console.log(json);
    getFirst5Followers(json.followers_url);
  })
  .catch(err => console.log(`falló: ${err.message}`))
};

github('santiaguf');
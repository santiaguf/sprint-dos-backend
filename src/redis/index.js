const redis = require('redis');
const client = redis.createClient();

client.on('error', function(error){
  console.error(error);
});

client.set('fecha', '2021-10-15', 'EX', 2);
client.set('encuentro', 31, redis.print);
client.set('tiempo_diversion', 'fin de semana', 'EX', 10);
client.set('es_viernes', true);

client.get('tiempo_diversion', (error, response) => console.log(error ? error : response));

const result = client.get('encuentro', (error, response) => error ? error : response);

setTimeout(() =>{
  client.get('fecha', (error, response) => console.log(error ? error : response));
}, 3000);
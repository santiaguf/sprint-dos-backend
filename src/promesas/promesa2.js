setTimeout(() => {
  console.log('han pasado 3 segundos');
}, 3000);


let promesa1 = new Promise((resolve, reject) => {
  setTimeout(() => {
    resolve(`promesa resuelta`);
  }, 1000);
  setTimeout(() => {
    reject(`promesa rechazada`);
  }, 500)
});


promesa1.then((MensajeExitoso) => {
  console.log(MensajeExitoso);
}).catch((mensajeFallido) => {
  console.log(mensajeFallido);
});
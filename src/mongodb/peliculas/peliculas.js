const mongoose = require('./conexion');
const modeloPelicula = {
  titulo: String,
  director: String,
  genero: String,
  lanzamiento: Date
};
const Pelicula = mongoose.model('peliculas', modeloPelicula);

module.exports = Pelicula;
require('dotenv').config();
const jwt = require('jsonwebtoken');
const helmet = require("helmet");
const express = require('express');
const redis = require('redis');
const bluebird = require('bluebird');
bluebird.promisifyAll(redis);
const client = redis.createClient();
const funciones = require('./funciones');
const app = express();
const port = 3022;
const host = 'http://localhost';
const url = `${host}:${port}`;

app.use(express.urlencoded({ extended: true }));
app.use(express.json());

app.use(helmet());

const disCache = async (req, res, next) => {
  const dishesOnRedis = await client.getAsync('platos');
  dishesOnRedis !== null ? res.json(JSON.parse(dishesOnRedis)) : next();
}

const dishExists = (req, res, next) => {
  funciones.dishExists(req.params.id)
  .then((result) => result ? next() : res.status(404).send('Dish not found'))
  .catch(() => res.status(404).json('Dish not found'));
}

const isAdmin = (req, res, next) => {
  const bearer = req.headers.authorization.replace('Bearer ','');
  const token = bearer.replace('Bearer ','');
  const decoded = jwt.verify(token, process.env.JWT_SECRET);
  decoded.user === 'admin' ? next() : res.status(401).send('Unauthorized');
}

app.get('/', disCache, function (req, res) {
  funciones.listDishes()
  .then(dishes => res.json(dishes))
  .catch(err => res.json(err));
});

app.get('/:id', dishExists, function (req, res) {
  funciones.getDishById(req.params.id)
  .then(dishes => res.json(dishes))
  .catch(err => res.json(err));
});

app.post('/', isAdmin, function (req, res) {
  funciones.createDish(req.body)
  .then(dish => res.json(dish))
  .catch(err => res.json(err));
});

app.put('/:id', dishExists, function (req, res) {
  funciones.updateDish(req.params.id, req.body)
  .then(dish => res.json(dish))
  .catch(err => res.json(err));
});

app.delete('/:id', dishExists, function (req, res) {
  funciones.deleteDish(req.params.id)
  .then(dish => res.json(dish))
  .catch(err => res.json(err));
});

app.post('/login', function (req, res) {
  const response =  funciones.login(req.body);
  res.json(response);
})

app.listen(port, () => {
    console.log(`Servidor iniciado en ${url}`);
});
const userModel = require('../models/users');

const createPost = async (post) => await post.save();

const validateIfEmailExists = async (emailInUrl) => await userModel.exists({ email: emailInUrl });

const createUser = async () => {
  let userToBeCreated = {
    nombre: req.body.nombre,
    apellido: req.body.apellido,
    email: req.body.email
  }

  const newUser = new userModel(userToBeCreated);
  const result = await newUser.save();
  return result;
}

const searchUser = async () =>  await userModel.findOne({email: req.params.email});

module.exports = {
  createPost,
  validateIfEmailExists,
  createUser,
  searchUser
}
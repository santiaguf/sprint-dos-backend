const express = require('express');
const port = 3024;
const host = 'http://localhost';
const url = `${host}:${port}`;
const app = express();
const posteoModel = require('./models/publicaciones');
const functions = require('./controllers/functions');


const helloWorld = {
  titulo: 'Hola mundo',
  cuerpo: 'Te doy la bienvenida',
  fecha: new Date(),
};

const newPost = new posteoModel(helloWorld);

const demoComment = {
  titulo: 'genial',
  cuerpo: 'Este es un comentario positivo, porque es mayor a 0',
  fecha: new Date()
};

newPost.comentarios.push(demoComment);


functions.createPost(newPost)
  .then(response =>  console.log(response))
  .catch(error =>  console.log(error));


app.listen(port, () => {
    console.log(`Servidor corriendo en ${url}`);
});

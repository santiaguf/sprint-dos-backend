const express = require('express');
const app = express();
const port = 3025;
const host = 'http://localhost';
const url = `${host}:${port}`;
const functions = require('./controllers/functions');

app.use(express.urlencoded({ extended: true }));
app.use(express.json());

const emailExists = (req, res, next) => {
  functions.validateIfEmailExists(req.params.email)
  .then((result) => result ? next() : res.status(404).send('email not found'))
  .catch(() => res.status(404).json('email not found'));
}

const emailInUse = (req, res, next) => {
  functions.validateIfEmailExists(req.body.email)
  .then((result) => result ? res.status(404).send('email in use') : next())
  .catch(() => res.status(404).json('error'));
}

app.post('/', emailInUse, (req, res) => {
  functions.createUser()
  .then(result => res.json(result))
  .catch(err => res.json(err));
});

app.post('/:email', emailExists, (req, res) => {
  functions.searchUser()
    .then(user => {
      user.contacto.push({
        telefono: req.body.telefono,
        instagram: req.body.instagram,
        Direccion: req.body.Direccion
      });
      user.save();
      res.json(user);
    })
    .catch(err => res.json(err));
});

app.listen(port, () => console.log(`Servidor corriendo en ${url}`));
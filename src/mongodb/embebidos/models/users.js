const mongoose = require('../../conexion');
const { Schema } = mongoose;

const schemaContact = new Schema({
  telefono: Number,
  instagram: String,
  Direccion: String
});

const schemaUser = new Schema({
  nombre: String,
  apellido: String,
  email: String,
  contacto: [schemaContact]
});

const userModel = mongoose.model('usuarios', schemaUser);

module.exports = userModel;
const mongoose = require('../../conexion');
const { Schema } = mongoose;

const Comentario = new Schema({
  titulo: String,
  cuerpo: String,
  fecha: Date
});

const Posteo = new Schema({
  titulo: String,
  cuerpo: String,
  fecha: Date,
  comentarios: [Comentario]
});

const posteoModel = mongoose.model('posteos', Posteo);


module.exports = posteoModel;